package dam1819.gats.classes;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

@Root(name = "animal")
public class Animal {

    @ElementList(required = true, inline = true)
    private ArrayList<Gos> gossos;
    @ElementList(required = true, inline = true)
    private ArrayList<Gat> gats;

    public Animal(){
        this.gats = new ArrayList<Gat>();
        this.gossos = new ArrayList<Gos>();
    }

    public String toString(){
        String str = this.gats.get(0).getNom();
        String str2 = this.gossos.get(0).getNom();

        return str+str2;
    }

}
