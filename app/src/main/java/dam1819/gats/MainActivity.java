package dam1819.gats;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.IOException;
import java.io.InputStream;

import dam1819.gats.classes.Gat;

public class MainActivity extends AppCompatActivity implements GatFragment.OnListFragmentInteractionListener{

    private TextView tv_nom;
    private TextView tv_sexe;
    private TextView tv_edat;
    private TextView tv_rassa;
    private TextView tv_propietari;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*this.tv_nom = (TextView) findViewById(R.id.tv_nom);
        this.tv_sexe = (TextView) findViewById(R.id.sexe);
        this.tv_edat = (TextView) findViewById(R.id.tv_edat);
        this.tv_rassa = (TextView) findViewById(R.id.rassa);
        this.tv_propietari = (TextView) findViewById(R.id.propietari);*/

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llegirXML();
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        changeFragment(GatFragment.class);
    }
    private void changeFragment(Class fragmentClass){
        Fragment fragment = null;

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_content, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    private void llegirXML() {

        AssetManager am = getAssets();
        InputStream input = null;

        try {
            input = am.open("gat.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Serializer ser = new Persister();
        Gat gat = null;

        try {
            gat = ser.read(Gat.class, input);
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.tv_nom.setText(gat.getNom());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListFragmentInteraction(Gat gat) {

    }
}
