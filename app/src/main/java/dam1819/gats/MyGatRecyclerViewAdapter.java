package dam1819.gats;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import dam1819.gats.GatFragment.OnListFragmentInteractionListener;
import dam1819.gats.classes.Gat;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link dam1819.gats.classes.Gat} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyGatRecyclerViewAdapter extends RecyclerView.Adapter<MyGatRecyclerViewAdapter.ViewHolder> {

    private final List<Gat> gats;
    private final OnListFragmentInteractionListener mListener;

    public MyGatRecyclerViewAdapter(List<Gat> items, OnListFragmentInteractionListener listener) {
        gats = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_gat, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = gats.get(position);
        holder.tv_nom.setText(gats.get(position).getNom());
        holder.tv_edat.setText(gats.get(position).getEdat());
        holder.tv_rassa.setText(gats.get(position).getRassa());
        holder.tv_sexe.setText(gats.get(position).getSexe());
        holder.tv_propietari.setText(gats.get(position).getPropietari());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return gats.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tv_nom;
        public final TextView tv_edat;
        public final TextView tv_sexe;
        public final TextView tv_rassa;
        public final TextView tv_propietari;
        public Gat mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tv_nom = (TextView) view.findViewById(R.id.nom);
            tv_edat = (TextView) view.findViewById(R.id.edat);
            tv_sexe = (TextView) view.findViewById(R.id.sexe);
            tv_rassa = (TextView) view.findViewById(R.id.rassa);
            tv_propietari = (TextView) view.findViewById(R.id.propietari);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tv_edat.getText() + "'";
        }
    }
}
