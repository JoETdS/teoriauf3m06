package dam1819.gats;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import dam1819.gats.classes.Animal;
import dam1819.gats.classes.Gat;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class GatFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    private ArrayList<Gat> gats;

    private MyGatRecyclerViewAdapter myGatRecyclerViewAdapter;

    private TextView animal;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public GatFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static GatFragment newInstance(int columnCount) {
        GatFragment fragment = new GatFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.gats = new ArrayList<Gat>();

        //Gat gat = llegirXML();
        Gat gat = new Gat("a",3,"c","d","e");
        this.gats.add(gat);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    private void llegirXMLAnimals() {

        AssetManager am = getActivity().getAssets();
        InputStream input = null;

        try {
            input = am.open("animals.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Serializer ser = new Persister();
        Animal animal = null;

        try {
            animal = ser.read(Animal.class, input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.animal.setText(animal.toString());

    }

    private Gat llegirXML() {
        AssetManager am = getActivity().getAssets();
        InputStream input = null;

        try {
            input = am.open("gat.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Serializer ser = new Persister();
        Gat gat = null;

        try {
            gat = ser.read(Gat.class, input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gat;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gat_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            this.myGatRecyclerViewAdapter =new MyGatRecyclerViewAdapter(this.gats, mListener);
            recyclerView.setAdapter(this.myGatRecyclerViewAdapter);
        }

        this.animal = (TextView) getActivity().findViewById(R.id.animal);

        llegirXMLAnimals();

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Gat gat);
    }
}
