package dam1819.gats.classes;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "gos")
public class Gos {

    @Element(name = "nom")
    private String nom;
    @Element(name = "edat")
    private int edat;
    @Element(name = "sexe")
    private String sexe;
    @Element(name = "rassa")
    private String rassa;
    @Element(name = "propietari")
    private String propietari;

    public Gos(String nom, int edat, String sexe, String rassa, String propietari) {
        this.nom = nom;
        this.edat = edat;
        this.sexe = sexe;
        this.rassa = rassa;
        this.propietari = propietari;
    }

    private Gos(){}

    public String getNom() {
        return nom;
    }

    public String getEdat() {
        return String.valueOf(this.edat);
    }

    public String getSexe() {
        return sexe;
    }

    public String getRassa() {
        return rassa;
    }

    public String getPropietari() {
        return propietari;
    }

}
